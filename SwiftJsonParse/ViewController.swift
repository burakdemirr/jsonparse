//
//  ViewController.swift
//  SwiftJsonParse
//
//  Created by burak on 24.02.2020.
//  Copyright © 2020 burak. All rights reserved.
//

import UIKit
import SDWebImage
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    var head = [String]()
    var images = [String]()
    var expriesDate = [String]()
    
    let now:Int32 = Int32(NSDate().timeIntervalSince1970) + 3*60*60
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return head.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! CampTableViewCell
    
        cell.campName.text = head[indexPath.row]
        cell.remainTime.text = expriesDate[indexPath.row]
        
//        let imageView = cell.viewWithTag(124) as! UIImageView
//        imageView.sd_setImage(with: URL(string: images[indexPath.row]))
        
//        let diffDate = Double(Int((self.expriesDate[indexPath.row]))) - Double(self.now)
//        let days = floor(Double(diffDate/86400))
//        let hours = floor(Double(diffDate-days*86400)/(60*60))
//        cell.remainTime.text = "\(Int(days)) gun \(Int(hours)) saat"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("the row selected")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let urlString = "http://www.bucayapimarket.com/json.php" //hata duzeltildi
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error != nil {
                print(error!)
            }
            else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray
                    
                    if let jsonDic = json {
                        for i in 0..<jsonDic.count{
                            if let jsonObjects = jsonDic[i] as? NSDictionary{
//                                self.head.append(jsonObjects["baslik"] as! String)
//                                self.images.append(jsonObjects["resim"] as! String)
//                                self.expriesDate.append(jsonObjects["tarih"] as! String)
//                                print(self.head)
                                if let headArr = jsonObjects["baslik"] as? String{
                                    self.head.append(headArr)
                                }
                                if let dateArr = jsonObjects["tarih"] as? String{
                                    self.expriesDate.append(dateArr)
                                }
                                if let imgArr = jsonObjects["resim"] as? String{
                                    self.images.append(imgArr)
                                }
                            }
                        }
                       
                    }
                    
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
                catch{
                    print(error)
                }
            }
        }
        task.resume()
    }


}


