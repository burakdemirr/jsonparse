//
//  CampTableViewCell.swift
//  SwiftJsonParse
//
//  Created by burak on 25.02.2020.
//  Copyright © 2020 burak. All rights reserved.
//

import UIKit

class CampTableViewCell: UITableViewCell {

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var campName: UILabel!
    @IBOutlet weak var remainTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
